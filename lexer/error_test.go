package lexer_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestError(t *testing.T) {
	r := require.New(t)
	e := lexer.Err{
		Source:  "test",
		Line:    1,
		Column:  10,
		Code:    "SELECT\t\t-",
		Message: "message",
	}
	s := e.Report()
	r.Equal(
		"message at +1:10 test\n"+
			"SELECT        -\n"+
			"               ^", s)
}

func TestWriteCarret(t *testing.T) {
	r := require.New(t)
	s := lexer.WriteCarret("\n\t--\nSELECT\t\t-", 9)
	r.Equal(`
    --
SELECT        -
              ^`, s)
}
