package lexer

type Token struct {
	Type   TokenType
	Prefix string `yaml:"-"`          // spaces and comments
	Raw    string `yaml:",omitempty"` // raw value
	Suffix string `yaml:"-"`          // spaces and comments up to EOL

	// Position in the input string
	Line   int `yaml:"-"`
	Column int `yaml:"-"`

	Str   string `yaml:"-"` // normalized value. e.g. : unquoted string, identifier, uppercased keyword, etc.
	Error error  `yaml:"-"` // error if any
}

func (t Token) String() string {
	return t.Raw
}

type TokenType int

const (
	EOF TokenType = iota
	Error
	Float
	Identifier
	Integer
	Keyword
	Operator
	Punctuation
	String
)

func (t TokenType) MarshalYAML() (interface{}, error) {
	switch t {
	case EOF:
		return "EOF", nil
	case Error:
		return "Error", nil
	case Float:
		return "Float", nil
	case Identifier:
		return "Identifier", nil
	case Integer:
		return "Integer", nil
	case Keyword:
		return "Keyword", nil
	case Operator:
		return "Operator", nil
	case Punctuation:
		return "Punctuation", nil
	case String:
		return "String", nil
	default:
		panic("unknown token type")
	}
}
