package lexer_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestEmpty(t *testing.T) {
	r := require.New(t)
	l := lexer.New("<stdin>", "")
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 1, Column: 1}, l.Next())
	r.Panics(func() { l.Next() })
}

func TestPrefix(t *testing.T) {
	r := require.New(t)
	l := lexer.New("", " \tSELECT")
	r.Equal(lexer.Token{Prefix: " \t", Raw: "SELECT", Type: lexer.Keyword, Line: 1, Column: 3}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 1, Column: 9}, l.Next())
	r.Panics(func() { l.Next() })

	l = lexer.New("", "-- prefix comment\n SELECT")
	r.Equal(lexer.Token{Prefix: "-- prefix comment\n ", Raw: "SELECT", Type: lexer.Keyword, Line: 2, Column: 2}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 2, Column: 8}, l.Next())

	l = lexer.New("", "-- com\n\n\n")
	r.Equal(lexer.Token{Prefix: "-- com\n\n\n", Type: lexer.EOF, Line: 4, Column: 1}, l.Next())
}

func TestSuffix(t *testing.T) {
	r := require.New(t)
	l := lexer.New("", "SELECT\n")
	r.Equal(lexer.Token{Raw: "SELECT", Suffix: "\n", Type: lexer.Keyword, Line: 1, Column: 1}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 2, Column: 1}, l.Next())

	l = lexer.New("", "SELECT  \n")
	r.Equal(lexer.Token{Raw: "SELECT", Suffix: "  \n", Type: lexer.Keyword, Line: 1, Column: 1}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 2, Column: 1}, l.Next())

	l = lexer.New("", "SELECT  -- Comment\n")
	r.Equal(lexer.Token{Raw: "SELECT", Suffix: "  -- Comment\n", Type: lexer.Keyword, Line: 1, Column: 1}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 2, Column: 1}, l.Next())

	l = lexer.New("", "SELECT\n\n\n")
	r.Equal(lexer.Token{Raw: "SELECT", Suffix: "\n", Type: lexer.Keyword, Line: 1, Column: 1}, l.Next())
	r.Equal(lexer.Token{Prefix: "\n\n", Type: lexer.EOF, Line: 4, Column: 1}, l.Next())
}

func TestKeywords(t *testing.T) {
	r := require.New(t)
	l := lexer.New("<stdin>", "SELECT;")
	r.Equal(lexer.Token{Raw: "SELECT", Type: lexer.Keyword, Line: 1, Column: 1}, l.Next())
	r.Equal(lexer.Token{Raw: ";", Type: lexer.Punctuation, Line: 1, Column: 7}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 1, Column: 8}, l.Next())
}

func TestIdentifier(t *testing.T) {
	r := require.New(t)
	l := lexer.New("", `select sysDate as "identifier";`)
	r.Equal(lexer.Token{Raw: "select", Type: lexer.Keyword, Line: 1, Column: 1}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "sysDate", Type: lexer.Identifier, Line: 1, Column: 8}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "as", Type: lexer.Keyword, Line: 1, Column: 16}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: `"identifier"`, Type: lexer.Identifier, Line: 1, Column: 19}, l.Next())
	r.Equal(lexer.Token{Raw: ";", Type: lexer.Punctuation, Line: 1, Column: 31}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 1, Column: 32}, l.Next())

	l = lexer.New("", `select sysDate as "iden "" tifier";`)
	r.Equal(lexer.Token{Raw: "select", Type: lexer.Keyword, Line: 1, Column: 1}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "sysDate", Type: lexer.Identifier, Line: 1, Column: 8}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "as", Type: lexer.Keyword, Line: 1, Column: 16}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: `"iden "" tifier"`, Type: lexer.Identifier, Line: 1, Column: 19}, l.Next())
	r.Equal(lexer.Token{Raw: ";", Type: lexer.Punctuation, Line: 1, Column: 35}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 1, Column: 36}, l.Next())

	l = lexer.New("", `TO_DATE('2019-01-01', 'YYYY-MM-DD')`)
	r.Equal(lexer.Token{Raw: "TO_DATE", Type: lexer.Identifier, Line: 1, Column: 1}, l.Next())
	r.Equal(lexer.Token{Raw: "(", Type: lexer.Punctuation, Line: 1, Column: 8}, l.Next())
	r.Equal(lexer.Token{Raw: "'2019-01-01'", Type: lexer.String, Line: 1, Column: 9}, l.Next())
	r.Equal(lexer.Token{Raw: ",", Type: lexer.Punctuation, Line: 1, Column: 21}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "'YYYY-MM-DD'", Type: lexer.String, Line: 1, Column: 23}, l.Next())
	r.Equal(lexer.Token{Raw: ")", Type: lexer.Punctuation, Line: 1, Column: 35}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 1, Column: 36}, l.Next())
}

func TestStringLiteral(t *testing.T) {
	r := require.New(t)
	l := lexer.New("", `select 'string literal' as "identifier";`)
	r.Equal(lexer.Token{Raw: "select", Type: lexer.Keyword, Line: 1, Column: 1}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "'string literal'", Type: lexer.String, Line: 1, Column: 8}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "as", Type: lexer.Keyword, Line: 1, Column: 25}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: `"identifier"`, Type: lexer.Identifier, Line: 1, Column: 28}, l.Next())
	r.Equal(lexer.Token{Raw: ";", Type: lexer.Punctuation, Line: 1, Column: 40}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 1, Column: 41}, l.Next())

	l = lexer.New("", `select 'string '' literal';`)
	r.Equal(lexer.Token{Raw: "select", Type: lexer.Keyword, Line: 1, Column: 1}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "'string '' literal'", Type: lexer.String, Line: 1, Column: 8}, l.Next())
	r.Equal(lexer.Token{Raw: ";", Type: lexer.Punctuation, Line: 1, Column: 27}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 1, Column: 28}, l.Next())
}

func TestNumbers(t *testing.T) {
	r := require.New(t)
	l := lexer.New("", `1 2.3 .4`)
	r.Equal(lexer.Token{Raw: "1", Type: lexer.Integer, Line: 1, Column: 1}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "2.3", Type: lexer.Float, Line: 1, Column: 3}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: ".4", Type: lexer.Float, Line: 1, Column: 7}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 1, Column: 9}, l.Next())

	l = lexer.New("", `5..6`)
	r.Equal(lexer.Token{Raw: "5", Type: lexer.Integer, Line: 1, Column: 1}, l.Next())
	r.Equal(lexer.Token{Raw: "..", Type: lexer.Operator, Line: 1, Column: 2}, l.Next())
	r.Equal(lexer.Token{Raw: "6", Type: lexer.Integer, Line: 1, Column: 4}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 1, Column: 5}, l.Next())
}

func TestOperators(t *testing.T) {
	r := require.New(t)
	l := lexer.New("", `1 + 2`)
	r.Equal(lexer.Token{Raw: "1", Type: lexer.Integer, Line: 1, Column: 1}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "+", Type: lexer.Operator, Line: 1, Column: 3}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "2", Type: lexer.Integer, Line: 1, Column: 5}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 1, Column: 6}, l.Next())

	l = lexer.New("", `3 != 4`)
	r.Equal(lexer.Token{Raw: "3", Type: lexer.Integer, Line: 1, Column: 1}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "!=", Type: lexer.Operator, Line: 1, Column: 3}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "4", Type: lexer.Integer, Line: 1, Column: 6}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 1, Column: 7}, l.Next())
}
