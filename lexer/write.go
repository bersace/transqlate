package lexer

import "strings"

func Write(l []Token) string {
	b := strings.Builder{}
	for _, t := range l {
		b.WriteString(t.Prefix)
		b.WriteString(t.Raw)
		b.WriteString(t.Suffix)
	}
	return b.String()
}

// WriteLine returns the SQL line of the token at pos.
func WriteLine(tokens []Token, pos int) string {
	l := tokens[pos].Line
	var lineTokens []Token

	// Prepend previous token on the same line
	for i := pos - 1; i >= 0; i-- {
		t := tokens[i]
		if t.Line != l {
			break
		}
		lineTokens = append([]Token{t}, lineTokens...)
	}

	// Append next token on the same line
	for i := pos; i < len(tokens); i++ {
		t := tokens[i]
		if t.Line != l {
			break
		}
		lineTokens = append(lineTokens, t)
	}

	return Write(lineTokens)
}
