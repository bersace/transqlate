package lexer

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

type Lexer struct {
	Source string // Name of the input
	input  string // The input string
	state  stateFn
	pos    int // Current position in the input
	prefix int // Start position of token space and comments beforehand.
	start  int // Start position of this token
	line   int // \n count
	col    int // Current column
	tokens chan Token
}

// New creates a new lexer for the input string.
// The source parameter is used for error reporting.
func New(source, input string) *Lexer {
	return &Lexer{
		Source: source,
		input:  input,
		state:  lexScript,
		tokens: make(chan Token, 3),
	}
}

func (l *Lexer) Next() Token {
	for {
		select {
		case token := <-l.tokens:
			return token
		default:
			if l.state == nil {
				panic("lexer state is nil")
			}
			l.state = l.state(l)
		}
	}
}

// emit passes a token back to the client.
func (l *Lexer) emit(tt TokenType) {
	t := Token{
		Prefix: l.input[l.prefix:l.start],
		Raw:    l.input[l.start:l.pos],
		Type:   tt,
		Line:   1 + l.line,
		Column: 1 + l.col,
	}
	l.prefix = l.pos
	l.start = l.pos
	l.col += len(t.Raw)
	t.Suffix = l.suffix()
	if t.Type == Integer && t.Raw == "." {
		panic("integer token with a single dot")
	}

	l.tokens <- t
}

// suffix consumes spaces and comment up to new line or EOF, or nil.
func (l *Lexer) suffix() string {
	incomment := false
	start := l.pos
loop:
	for {
		switch r := l.next(); {
		case r == ' ' || r == '\t':
			continue loop
		case r == '-': // maybe a comment start ?
			n := l.peek()
			if n == '-' {
				l.next()
				incomment = true
			} else {
				break loop
			}
		case r == 0 || r == '\n': // We have a suffix.
			s := l.input[start:l.pos]
			if len(s) == 0 {
				return ""
			}
			l.start = l.pos
			l.prefix = l.pos
			if r == '\n' {
				l.line++
				l.col = 0
			}
			return s
		default:
			if incomment {
				// Accept anything inside comment
				continue loop
			} else {
				// Not a comment, this is next token.
				break loop
			}
		}
	}

	// There is a token next. Keep spaces for next token prefix.
	l.pos = start
	return ""
}

// next returns the next rune in the input.
// Returns 0 if the end of the input was reached.
func (l *Lexer) next() (r rune) {
	if l.pos >= len(l.input) {
		return 0
	}
	r, w := utf8.DecodeRuneInString(l.input[l.pos:])
	l.pos += w
	return r
}

// peek returns but does not consume the next rune in the input.
// Returns 0 if the end of the input was reached.
func (l *Lexer) peek() (r rune) {
	r = l.next()
	if r != 0 {
		l.backup()
	}
	return r
}

// backup steps back one rune. Can only be called once per call of next.
func (l *Lexer) backup() {
	_, w := utf8.DecodeLastRuneInString(l.input[:l.pos])
	l.pos -= w
}

// accept consumes the next rune if it's from the valid set.
func (l *Lexer) accept(valid string) bool {
	if strings.IndexRune(valid, l.next()) >= 0 {
		return true
	}
	l.backup()
	return false
}

// acceptMany consumes a set of runes from the valid set.
func (l *Lexer) acceptMany(valid string) {
	for {
		r := l.next()
		if r == 0 {
			return
		}
		if strings.IndexRune(valid, r) == -1 {
			l.backup()
			return
		}
	}
}

// acceptUntil consumes runes until the stop rune is encountered.
func (l *Lexer) acceptUntil(stop rune) {
	for {
		switch r := l.next(); {
		case r == 0:
			return
		case r == stop:
			l.backup()
			return
		}
	}
}

// ignore skips over the pending input before this point.
func (l *Lexer) ignore() {
	lines := strings.Count(l.input[l.start:l.pos], "\n")
	if lines > 0 {
		l.line += lines
		lastNewline := strings.LastIndex(l.input[:l.pos], "\n")
		l.col = len(l.input[lastNewline+1 : l.pos])
	} else {
		l.col += l.pos - l.start
	}
	l.start = l.pos
}

// currentLine returns the current line of the input.
func (l *Lexer) currentLine() string {
	lastNewline := strings.LastIndex(l.input[:l.pos-1], "\n")
	if lastNewline == -1 {
		lastNewline = 0
	} else {
		lastNewline++
	}
	nextNewline := strings.Index(l.input[l.pos-1:], "\n")
	if nextNewline == -1 {
		nextNewline = len(l.input)
	} else {
		nextNewline += l.pos
	}
	return strings.TrimRight(l.input[lastNewline:nextNewline], "\n")
}

// errorf returns an error token and terminates the scan by passing
// back a nil pointer that will be the next state, terminating l.Next().
func (l *Lexer) errorf(format string, args ...interface{}) stateFn {
	l.col += l.pos - l.start - 1
	e := Err{
		Source:  l.Source,
		Line:    1 + l.line,
		Column:  1 + l.col,
		Code:    l.currentLine(),
		Message: fmt.Sprintf(format, args...),
	}

	l.tokens <- Token{
		Type:  Error,
		Raw:   e.Message,
		Error: e,
	}
	return nil
}
