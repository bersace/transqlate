package lexer

import (
	"fmt"
	"strings"
)

type Reporter interface {
	Report() string
}

type Err struct {
	Source  string // Name of the input
	Line    int    // Line number in Source.
	Column  int    // Column number in Source.
	Code    string // The offending SQL line.
	Message string
}

func (e Err) Error() string {
	return e.Message
}

func (e Err) Report() string {
	return fmt.Sprintf(
		"%s at +%d:%d %s\n%s",
		e.Message, e.Line, e.Column, e.Source,
		WriteCarret(e.Code, e.Column),
	)
}

// WriteCarret returns the SQL line of the token at pos with a carret under the token.
func WriteCarret(line string, pos int) string {
	line = strings.TrimRight(line, "\n")

	var lastLine string
	lastLineBreak := strings.LastIndex(line, "\n")
	if lastLineBreak == -1 {
		lastLine = line
	} else {
		lastLine = line[lastLineBreak+1:]
	}
	caretPos := pos + 3*strings.Count(lastLine, "\t") - 1

	b := strings.Builder{}
	b.WriteString(strings.ReplaceAll(line, "\t", "    "))
	b.WriteString("\n")
	b.WriteString(strings.Repeat(" ", caretPos))
	b.WriteString("^")
	return b.String()
}
