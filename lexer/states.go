package lexer

import "strings"

type stateFn func(*Lexer) stateFn

const operators = "!&+,-/:;<=>@|"

// lexScript is the top-level state for lexing a script.
func lexScript(l *Lexer) stateFn {
	switch r := l.next(); {
	case r == 0:
		l.emit(EOF)
		return nil
	case r == ' ' || r == '\t' || r == '\n':
		l.ignore()
		return lexScript
	case r == '-':
		n := l.peek()
		if n == '-' {
			l.backup()
			return lexComment
		} else if n == 0 {
			return l.errorf("unexpected end of input after '-'")
		} else {
			return lexOperator
		}
	case strings.IndexRune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", r) >= 0:
		return lexKeywordOrIdentifier
	case r == '\'':
		return lexString
	case r == '"':
		return lexQuotedIdentifier
	case strings.IndexRune(".0123456789", r) >= 0:
		l.backup()
		return lexNumber
	case strings.IndexRune("*,;()[]{}", r) >= 0:
		l.emit(Punctuation)
		return lexScript
	case strings.IndexRune(operators, r) >= 0:
		return lexOperator
	default:
		l.errorf("unrecognized character: %#U", r)
		l.backup()
		return nil
	}
}

// lexComment consumes a comment.
func lexComment(l *Lexer) stateFn {
	l.acceptUntil('\n')
	l.ignore()
	return lexScript
}

// lexKeywordOrIdentifier consumes a keyword.
func lexKeywordOrIdentifier(l *Lexer) stateFn {
	l.acceptMany("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	word := strings.ToUpper(l.input[l.start:l.pos])
	if Keywords.Contains(word) {
		n := l.peek()
		if strings.IndexRune("_$", n) == -1 { // Don't consume TO_DATE as keyword TO and identifier _DATE.
			l.emit(Keyword)
			return lexScript
		}
	}

	l.acceptMany("_$abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	l.emit(Identifier)
	return lexScript
}

// lexQuotedIdentifier consumes an identifier.
func lexQuotedIdentifier(l *Lexer) stateFn {
	for {
		l.acceptUntil('"') // walk until the closing quote
		r := l.next()      // consume the quote
		if r == 0 {
			return l.errorf("unterminated quoted identifier")
		}
		r = l.peek()
		if r != '"' {
			// Unescaped quote, we're done.
			l.emit(Identifier)
			return lexScript
		}
		// Escaped quote, consume it and continue.
		l.next()
	}
}

// lexString consumes a string literal.
func lexString(l *Lexer) stateFn {
	for {
		l.acceptUntil('\'') // walk until the closing quote
		r := l.next()       // consume the quote
		if r == 0 {
			return l.errorf("unterminated string literal")
		}
		r = l.peek()
		if r != '\'' {
			// Unescaped quote, we're done.
			l.emit(String)
			return lexScript
		}
		// Escaped quote, consume it and continue.
		l.next()
	}
}

// lexNumber consumes a number or a DOT DOT range.
func lexNumber(l *Lexer) stateFn {
	l.acceptMany("0123456789")
	r := l.peek()
	if r == '.' { // float or range
		l.next()
		r := l.peek()
		if r == '.' {
			// This is a range operator, not a float.
			l.backup()
			l.emit(Integer)
			l.next()
			l.next()
			l.emit(Operator)
			l.acceptMany("0123456789")
			l.emit(Integer)
			return lexScript
		}
		l.acceptMany("0123456789")
		l.emit(Float)
		return lexScript
	}
	l.emit(Integer)
	return lexScript
}

// lexOperator consumes an operator.
func lexOperator(l *Lexer) stateFn {
	l.acceptMany(operators)
	l.emit(Operator)
	return lexScript
}
