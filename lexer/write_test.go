package lexer_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestWriteLine(t *testing.T) {
	r := require.New(t)
	l := lexer.New("", "1\n2 3 4\n5\n")
	var tokens []lexer.Token
	for {
		tok := l.Next()
		tokens = append(tokens, tok)
		if tok.Type == lexer.EOF {
			break
		}
	}
	r.Len(tokens, 6)

	r.Equal("3", tokens[2].Raw)
	sql := lexer.WriteLine(tokens, 2)
	r.Equal("2 3 4\n", sql)
}
