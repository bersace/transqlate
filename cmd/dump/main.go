package main

import (
	"bufio"
	"fmt"
	"io"
	"log/slog"
	"os"
	"strings"

	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/parser"
	"gopkg.in/yaml.v3"
)

func main() {
	slog.SetDefault(slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
		Level: slog.LevelDebug,
	})))

	if len(os.Args) != 2 {
		Fatal(fmt.Errorf("usage: %s <file>", os.Args[0]))
	}
	source := os.Args[1]
	sql := readSource(source)
	slog.Debug("Parsing SQL.", "source", source)
	tree, err := parser.Parse(lexer.New(source, sql))
	if err != nil {
		Fatal(err)
	}
	y, err := yaml.Marshal(tree)
	if err != nil {
		Fatal(err)
	}
	slog.Info("Dumping as YAML.")
	os.Stdout.Write(y)

	slog.Info("Serialize back to plain SQL.")
	os.Stdout.WriteString(lexer.Write(tree.Tokens()))
}

func readSource(name string) string {
	switch name {
	case "-":
		os.Stderr.WriteString("Type SQL snippet and terminate with ^D\n")
		return readFile(os.Stdin)
	case "@":
		slog.Debug("Using builtin sample.")
		sql := "SELECT 1\n"
		os.Stderr.WriteString(sql)
		return sql
	default:
		slog.Debug("Opening file.", "path", name)
		fd, err := os.Open(name)
		if err != nil {
			Fatal(err)
		}
		defer fd.Close()
		return readFile(fd)
	}
}

func readFile(fd io.Reader) string {
	b := strings.Builder{}
	br := bufio.NewReader(fd)
	for {
		chunk, _, err := br.ReadLine()
		if err == io.EOF {
			break
		} else if err != nil {
			Fatal(err)
		}
		b.Write(chunk)
		b.WriteByte('\n')
	}
	return b.String()
}

func Fatal(err error) {
	r, ok := err.(lexer.Reporter)
	if ok {
		os.Stderr.WriteString(r.Report())
	} else {
		os.Stderr.WriteString(err.Error())
	}
	os.Stderr.WriteString("\n")
	os.Exit(1)
}
