package rewrite

import (
	"gitlab.com/dalibo/transqlate/ast"
)

type Rule interface {
	Match(ast.Node) bool
	Rewrite(ast.Node) ast.Node
}

// Do applies all matching rules to rewrite an AST.
func Do(n ast.Node, rules []Rule) ast.Node {
	n = n.Walk(func(n ast.Node) ast.Node {
		for _, r := range rules {
			if r.Match(n) {
				n = r.Rewrite(n)
			}
		}
		return n
	})
	return n
}
