package rewrite

import (
	"fmt"
	"strings"

	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

type RewriteConstant struct {
	From, To string
}

var _ Rule = RewriteConstant{}

func (r RewriteConstant) String() string {
	return fmt.Sprintf("%s -> %s", r.From, r.To)
}

func (r RewriteConstant) Match(n ast.Node) bool {
	k, ok := n.(ast.Atom)
	if !ok {
		return false
	}
	if k.Token.Type != lexer.Identifier {
		return false
	}

	return k.Token.Str == r.From
}

func (r RewriteConstant) Rewrite(n ast.Node) ast.Node {
	k := n.(ast.Atom)
	k.Token.Raw = r.To
	k.Token.Str = strings.ToUpper(k.Token.Raw)
	return k
}

type RemoveLastArgument struct {
	Function string
}

func (r RemoveLastArgument) String() string {
	return fmt.Sprintf("remove last argument from %s", r.Function)
}

func (r RemoveLastArgument) Match(n ast.Node) bool {
	c, ok := n.(ast.Call)
	if !ok {
		return false
	}
	f, ok := c.Function.(ast.Atom)
	if !ok {
		// We don't support schema yet because such rules usually applies to builtin functions.
		return false
	}

	return f.Token.Str == r.Function
}

func (r RemoveLastArgument) Rewrite(n ast.Node) ast.Node {
	c := n.(ast.Call)
	a := c.Args.(ast.Grouping)
	list, ok := a.Body.(ast.Infix)
	if ok {
		a.Body = list.RemoveLast()
	} else {
		a.Body = nil
	}
	c.Args = a
	return c
}
