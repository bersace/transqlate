# Transpile any SQL to PostgreSQL Dialect

## Features

- preserve whitespaces, case and comments
- readable lexing and parsing error
- efficient AST and parsing using TDOP

`github.com/dalibo/transqlate` is available under PostgreSQL license.


## Authors

translate is a [Dalibo Labs] project.

[Dalibo Labs]: https://labs.dalibo.com/
