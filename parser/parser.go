package parser

import (
	"fmt"
	"slices"
	"strings"

	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

type Parser struct {
	input  *lexer.Lexer
	tokens []lexer.Token
	pos    int
}

// New creates a new parser.
func New(input *lexer.Lexer) *Parser {
	return &Parser{
		input: input,
	}
}

// Parse parses the input and returns a tree of nodes.
func Parse(l *lexer.Lexer) (ast.Node, error) {
	return New(l).Parse()
}

// Parse parses the input and returns the root of the AST.
func (p *Parser) Parse() (ast.Node, error) {
	for token := p.input.Next(); ; token = p.input.Next() {
		if token.Type == lexer.Error {
			return nil, token.Error
		}
		if token.Type == lexer.Keyword {
			// Uppercase keywords.
			token.Str = strings.ToUpper(token.Raw)
		} else if token.Type == lexer.Identifier {
			t := token.Raw
			if t[0] == '"' {
				// Unquote identifiers.
				t = t[1 : len(t)-1]
				t = strings.ReplaceAll(t, `""`, `"`)
			} else {
				// Lowercase unquoted identifiers, like PostgreSQL.
				t = strings.ToLower(token.Raw)
			}
			token.Str = t
		}

		p.tokens = append(p.tokens, token)

		if token.Type == lexer.EOF {
			break
		}
	}
	return p.parseSnippet()
}

// errorf returns an error at the current position.
func (p *Parser) errorf(format string, args ...interface{}) error {
	code := lexer.WriteLine(p.tokens, p.pos)
	return lexer.Err{
		Source:  p.input.Source,
		Line:    p.tokens[p.pos].Line,
		Column:  p.tokens[p.pos].Column,
		Code:    code,
		Message: fmt.Sprintf(format, args...),
	}
}

// consume step the next token and returns it.
func (p *Parser) consume() lexer.Token {
	token := p.tokens[p.pos]
	p.pos++
	return token
}

// backup rewinds the parser by one token.
func (p *Parser) backup() {
	p.pos--
}

// seek returns true if the next token matches the given type and value.
// If value is empty, only the type is checked.
func (p *Parser) seek(t lexer.TokenType, v string) bool {
	if p.tokens[p.pos].Type != t {
		return false
	}
	if v == "" {
		return true
	}
	return p.tokens[p.pos].Raw == v
}

// peek returns the next token without consuming it.
func (p *Parser) peek() lexer.Token {
	return p.tokens[p.pos]
}

// keywords returns true if the next tokens matches the given keywords in order.
// Does not consume the tokens.
// keyword is case insensitive.
func (p *Parser) keywords(v ...string) bool {
	for i, k := range v {
		t := p.tokens[p.pos+i]
		if t.Type != lexer.Keyword {
			return false
		}
		if t.Str != k {
			return false
		}
	}
	return true
}

// operators returns true if the next token is a punctuation within the given value.
func (p *Parser) operators(v ...string) bool {
	t := p.tokens[p.pos]
	if t.Type != lexer.Operator {
		return false
	}
	return slices.Contains(v, t.Raw)
}

// punctuations returns true if the next token is a punctuation within the given value.
func (p *Parser) punctuations(v ...string) bool {
	t := p.tokens[p.pos]
	if t.Type != lexer.Punctuation {
		return false
	}
	return slices.Contains(v, t.Raw)
}
