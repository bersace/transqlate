package parser

// lookaheadPrecedence returns the precedence of the next token.
// Returns 0 if the next token is another expression.
// This is used to implement the Pratt algorithm.
// See https://matklad.github.io/2020/04/13/simple-but-powerful-pratt-parsing.html
// for more information.
//
// Unlike the original Pratt algorithm, we also handle punctuations like comma, parenthesis, etc.
func (p *Parser) lookaheadPrecedence() int {
	switch {
	case p.punctuations("(", "["):
		return 100
	case p.punctuations("*"), p.operators("/", "%"), p.keywords("DIV"), p.keywords("MOD"):
		return 40
	case p.operators("+", "-"):
		return 30
	case p.operators("<", "=<", "!=", "=", "==", ">=", ">"):
		return 20
	case p.punctuations(","):
		return 15
	case p.keywords("AND"):
		return 10
	case p.keywords("OR"):
		return 5
	}
	return 0
}
