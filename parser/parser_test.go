package parser_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/parser"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestExpression(t *testing.T) {
	r := require.New(t)
	tree, e := parser.Parse(lexer.New("", "SYSDATE"))
	r.Nil(e)
	i := tree.(ast.Atom)
	r.Equal("SYSDATE", i.Token.Raw)
	r.Equal(lexer.Identifier, i.Token.Type)

	tree, e = parser.Parse(lexer.New("", "+1"))
	r.Nil(e)
	number := tree.(ast.Signed)
	r.Equal("+", number.Sign.Raw)
	r.Equal("1", number.Number.Raw)

	tree, e = parser.Parse(lexer.New("", "2 + 3"))
	r.Nil(e)
	plus := tree.(ast.Infix)
	r.Equal("+", plus.Op.Raw)
}

func TestPrecedence(t *testing.T) {
	r := require.New(t)
	tree, e := parser.Parse(lexer.New("", "1 + 2 * 3"))
	r.Nil(e)
	plus := tree.(ast.Infix)
	r.Equal("+", plus.Op.Raw)
	// The right operand is not 5, but the result of the multiplication.
	mul := plus.Right.(ast.Infix)
	r.Equal("*", mul.Op.Raw)

	tree, e = parser.Parse(lexer.New("", "4 * 5 + 6"))
	r.Nil(e)
	// The + is upper in the tree.
	plus = tree.(ast.Infix)
	r.Equal("+", plus.Op.Raw)
	// The left operand is not 5, but the result of the multiplication.
	mul = plus.Left.(ast.Infix)
	r.Equal("*", mul.Op.Raw)
}

func TestAndOr(t *testing.T) {
	r := require.New(t)
	tree, e := parser.Parse(lexer.New("", "1 = 2 AND 3 > 4 OR 5 != 6"))
	r.Nil(e)
	or := tree.(ast.Infix)
	r.Equal("OR", or.Op.Raw)
	// The left operand is not 1, but the result of the AND.
	and := or.Left.(ast.Infix)
	r.Equal("AND", and.Op.Raw)
}

func TestStatements(t *testing.T) {
	r := require.New(t)
	l := lexer.New("", "select SYSDATE;")
	tree, e := parser.Parse(l)
	r.Nil(e)
	s := tree.(ast.Statement)
	q := s.Expression.(ast.Select)
	r.Equal(";", s.End.Raw)
	r.Equal("select", q.Select.Raw)
}
