package parser

import (
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

// parseExpression parses a single expression.
// An expression is either a query, a prefix or infix operation or an atom.
func (p *Parser) parseExpression(precedence int) (ast.Node, error) {
	switch {
	case p.seek(lexer.EOF, ""):
		return nil, p.errorf("unexpected end of file")
	case p.operators("+", "-"):
		return p.parseSignedNumber()
	case p.keywords("SELECT"):
		return p.parseSelect()
	case p.punctuations("(", "["):
		return p.parseGrouping()
	case p.seek(lexer.Identifier, ""), p.seek(lexer.String, ""), p.seek(lexer.Integer, ""), p.seek(lexer.Float, ""):
		expr := p.parseAtom()
		for {
			next_precedence := p.lookaheadPrecedence()
			if precedence >= next_precedence {
				break
			}
			var new_expr ast.Node
			var err error
			if p.punctuations("(") {
				new_expr, err = p.parseCall(expr)
				if err != nil {
					return nil, p.errorf("call: %s", err)
				}
			} else {
				new_expr, err = p.parseInfix(expr, next_precedence)
				if err != nil {
					return nil, p.errorf("expression: %s", err)
				}
			}
			expr = new_expr
		}
		return expr, nil
	default:
		return nil, p.errorf("expression: unexpected token: %s", p.tokens[p.pos])
	}
}

// parseCall parses a function call.
func (p *Parser) parseCall(f ast.Node) (ast.Node, error) {
	args, err := p.parseGrouping()
	if err != nil {
		return nil, p.errorf("args: %s", err)
	}
	return ast.Call{
		Function: f,
		Args:     args,
	}, nil
}

// parseSignedNumber
func (p *Parser) parseSignedNumber() (ast.Node, error) {
	s := ast.Signed{
		Sign:   p.consume(),
		Number: p.consume(),
	}
	switch s.Number.Type {
	case lexer.Integer, lexer.Float:
		return s, nil
	default:
		return nil, p.errorf("expected number, got %s", s.Number)
	}
}

// parseInfix parses an infix operation with a single token operator.
func (p *Parser) parseInfix(left ast.Node, precedence int) (ast.Node, error) {
	n := ast.Infix{
		Left: left,
		Op:   p.consume(),
	}
	right, err := p.parseExpression(precedence)
	if err != nil {
		return nil, p.errorf("%s: %s", n.Op.Raw, err)
	}
	n.Right = right
	return n, nil
}

// parseAtom parses an identifier.
func (p *Parser) parseAtom() ast.Node {
	return ast.Atom{
		Token: p.consume(),
	}
}

// parseGrouping parses a grouping expression.
func (p *Parser) parseGrouping() (ast.Node, error) {
	open := p.consume()
	body, err := p.parseExpression(0)
	if err != nil {
		return nil, p.errorf("grouping: %s", err)
	}
	end := p.consume()
	return ast.Grouping{
		Open: open,
		Body: body,
		End:  end,
	}, nil
}
