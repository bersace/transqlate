package parser

import (
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

// parseSelect parses a SELECT statement.
func (p *Parser) parseSelect() (ast.Node, error) {
	selectToken := p.consume()
	if selectToken.Type != lexer.Keyword || selectToken.Str != "SELECT" {
		return nil, p.errorf("expected SELECT, got %s", selectToken)
	}
	l, err := p.parseExpression(0)
	if err != nil {
		return nil, err
	}
	var f ast.Node
	if p.keywords("FROM") {
		var err error
		f, err = p.parseFrom()
		if err != nil {
			return nil, p.errorf("from: %s", err)
		}
	}
	return ast.Select{
		Select: selectToken,
		List:   l,
		From:   f,
	}, nil
}

// parseFrom parses a FROM clause.
func (p *Parser) parseFrom() (ast.Node, error) {
	fromToken := p.consume()
	if fromToken.Type != lexer.Keyword || fromToken.Str != "FROM" {
		return nil, p.errorf("expected FROM, got %s", fromToken)
	}
	var l ast.Node
	if p.seek(lexer.Identifier, "DUAL") {
		l = ast.Atom{Token: p.consume()}
	} else {
		return nil, p.errorf("unhandled from syntax")
	}
	return ast.From{
		From: fromToken,
		List: l,
	}, nil
}
