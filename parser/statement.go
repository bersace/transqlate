package parser

import (
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

// parseSnippet parses a snippet of SQL.
// A snippet is either an expression or a list of statements.
func (p *Parser) parseSnippet() (ast.Node, error) {
	s := ast.Statements{}
	for {
		switch {
		case p.seek(lexer.EOF, ""):
			s.EOF = p.consume()
			if len(s.Statements) == 1 && s.EOF.Prefix == "" {
				return s.Statements[0], nil
			}
			return s, nil
		default:
			stmt, err := p.parseStatement()
			if err != nil {
				return nil, err
			}
			s.Statements = append(s.Statements, stmt)
		}
	}
}

// parseStatement parses a statement until ;
func (p *Parser) parseStatement() (ast.Node, error) {
	s := ast.Statement{}

	for {
		switch {
		case p.punctuations(";"):
			s.End = p.consume()
			return s, nil
		case p.seek(lexer.EOF, ""):
			// If statement without trailing semi-colon, we return the inner expression directly.
			return s.Expression, nil
		default:
			if s.Expression != nil {
				// We already consumed one expression. We expect a semi-colon now.
				return nil, p.errorf("unexpected token: %s", p.tokens[p.pos])
			}
			expr, err := p.parseExpression(0)
			if err != nil {
				return nil, err
			}
			s.Expression = expr
		}
	}
}
