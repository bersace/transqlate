package parser_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/parser"
)

func TestSelect(t *testing.T) {
	r := require.New(t)
	l := lexer.New("", "select SYSDATE FROM DUAL")
	tree, e := parser.Parse(l)
	r.Nil(e)
	s := tree.(ast.Select)
	r.Equal("select", s.Select.Raw)
	i := s.List.(ast.Atom)
	r.Equal("SYSDATE", i.Token.Raw)
	r.Equal("DUAL", s.From.(ast.From).List.(ast.Atom).Token.Raw)
}
