package parser_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/parser"
)

func TestParens(t *testing.T) {
	r := require.New(t)
	p := parser.New(lexer.New("<stdin>", "(1 + 2)"))
	n, err := p.Parse()
	r.NoError(err)
	g := n.(ast.Grouping)
	r.Equal("(", g.Open.Raw)
}

func TestCall(t *testing.T) {
	r := require.New(t)
	p := parser.New(lexer.New("<stdin>", "foo(1, 2) + 3"))
	n, err := p.Parse()
	r.NoError(err)
	// plus operator is at the top level
	plus := n.(ast.Infix)
	r.Equal("+", plus.Op.Raw)
	c := plus.Left.(ast.Call)

	f := c.Function.(ast.Atom)
	r.Equal("foo", f.Token.Raw)
	parens := c.Args.(ast.Grouping)
	r.Equal("(", parens.Open.Raw)
	args := parens.Body.(ast.Infix)
	r.Equal(",", args.Op.Raw)
	a := args.Left.(ast.Atom)
	r.Equal("1", a.Token.Raw)
	a = args.Right.(ast.Atom)
	r.Equal("2", a.Token.Raw)
	a = plus.Right.(ast.Atom)
	r.Equal("3", a.Token.Raw)
}
