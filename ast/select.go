package ast

import "gitlab.com/dalibo/transqlate/lexer"

type Select struct {
	Select lexer.Token
	List   Node
	From   Node
}

func (s Select) Tokens() []lexer.Token {
	t := []lexer.Token{s.Select}
	if s.List != nil {
		t = append(t, s.List.Tokens()...)
	}
	if s.From != nil {
		t = append(t, s.From.Tokens()...)
	}
	return t
}

func (s Select) Walk(f Visitor) Node {
	s.List = s.List.Walk(f)
	return f(s)
}

type From struct {
	From lexer.Token
	List Node
}

func (f From) Tokens() []lexer.Token {
	t := []lexer.Token{f.From}
	if f.List != nil {
		t = append(t, f.List.Tokens()...)
	}
	return t
}

func (f From) Walk(v Visitor) Node {
	f.List = f.List.Walk(v)
	return v(f)
}
