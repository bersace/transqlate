package ast_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestRemoveLast(t *testing.T) {
	r := require.New(t)
	// Tree for 1, 2, 3 + 4
	l := ast.Infix{
		Left: ast.Atom{Token: lexer.Token{Raw: "1"}},
		Op:   lexer.Token{Raw: ","},
		Right: ast.Infix{
			Left: ast.Atom{Token: lexer.Token{Raw: "2"}},
			Op:   lexer.Token{Raw: ","},
			Right: ast.Infix{
				Left:  ast.Atom{Token: lexer.Token{Raw: "3"}},
				Op:    lexer.Token{Raw: "+"},
				Right: ast.Atom{Token: lexer.Token{Raw: "4"}},
			},
		},
	}

	n := l.RemoveLast()
	s := lexer.Write(n.Tokens())
	r.Equal("1,2", s)
}
