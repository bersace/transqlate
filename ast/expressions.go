package ast

import (
	"gitlab.com/dalibo/transqlate/lexer"
)

// Atom is a single token node.
type Atom struct {
	Token lexer.Token
}

func (a Atom) Tokens() []lexer.Token {
	return []lexer.Token{a.Token}
}

func (a Atom) Walk(f Visitor) Node {
	return f(a)
}

type Signed struct {
	Sign   lexer.Token
	Number lexer.Token
}

func (s Signed) Tokens() []lexer.Token {
	return []lexer.Token{
		s.Sign,
		s.Number,
	}
}

func (s Signed) Walk(f Visitor) Node {
	return f(s)
}

// Infix is a node with an operator with left and right operands.
type Infix struct {
	Left  Node
	Op    lexer.Token
	Right Node
}

func (c Infix) Tokens() []lexer.Token {
	t := []lexer.Token{}
	t = append(t, c.Left.Tokens()...)
	t = append(t, c.Op)
	t = append(t, c.Right.Tokens()...)
	return t
}

func (c Infix) Walk(f Visitor) Node {
	c.Left = c.Left.Walk(f)
	c.Right = c.Right.Walk(f)
	return f(c)
}

// RemoveLast removes the deepest right node of an infix operation tree.
func (c Infix) RemoveLast() Node {
	r, ok := c.Right.(Infix)
	if !ok {
		// There is no following infix, remove the right node.
		return c.Left
	}
	if r.Op.Raw != c.Op.Raw {
		// The following infix is another operator, e.g 1, 2 + 3. Remove the right node.
		return c.Left
	}
	c.Right = r.RemoveLast()
	return c
}

// Grouping holds parenthesis, brackets, etc. around an expression.
type Grouping struct {
	Open lexer.Token
	Body Node
	End  lexer.Token
}

func (g Grouping) Tokens() []lexer.Token {
	t := []lexer.Token{}
	t = append(t, g.Open)
	t = append(t, g.Body.Tokens()...)
	t = append(t, g.End)
	return t
}

func (g Grouping) Walk(f Visitor) Node {
	g.Body = g.Body.Walk(f)
	return f(g)
}

type Call struct {
	Function Node
	// The grouping containing arguments
	Args Node
}

func (c Call) Tokens() []lexer.Token {
	t := []lexer.Token{}
	t = append(t, c.Function.Tokens()...)
	t = append(t, c.Args.Tokens()...)
	return t
}

func (c Call) Walk(f Visitor) Node {
	c.Function = c.Function.Walk(f)
	c.Args = c.Args.Walk(f)
	return f(c)
}
