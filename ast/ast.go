// AST for editing SQL code
package ast

import (
	"gitlab.com/dalibo/transqlate/lexer"
)

// Node is the base interface for all nodes in the AST.
type Node interface {
	Tokens() []lexer.Token
	Walk(Visitor) Node
}

type Visitor func(Node) Node

type Statements struct {
	Statements []Node
	EOF        lexer.Token
}

func (s Statements) Tokens() []lexer.Token {
	t := []lexer.Token{}
	for _, s := range s.Statements {
		t = append(t, s.Tokens()...)
	}
	t = append(t, s.EOF)
	return t
}

func (s Statements) Walk(f Visitor) Node {
	s2 := Statements{
		EOF: s.EOF,
	}
	for _, s := range s.Statements {
		s = s.Walk(f)
		if s != nil {
			s2.Statements = append(s2.Statements, s)
		}
	}
	return f(s2)
}

type Statement struct {
	Expression Node
	End        lexer.Token // The final semi-colon.
}

func (s Statement) Tokens() []lexer.Token {
	t := []lexer.Token{}
	t = append(t, s.Expression.Tokens()...)
	t = append(t, s.End)
	return t
}

func (s Statement) Walk(f Visitor) Node {
	s.Expression = s.Expression.Walk(f)
	return f(s)
}
