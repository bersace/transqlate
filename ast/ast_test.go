package ast_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestComma(t *testing.T) {
	r := require.New(t)
	var c ast.Node = ast.Infix{
		Left:  ast.Atom{lexer.Token{Raw: "SYSDATE", Type: lexer.Keyword}},
		Op:    lexer.Token{Raw: ",", Suffix: "\n", Type: lexer.Punctuation},
		Right: ast.Atom{lexer.Token{Raw: "CURRENT_TIMESTAMP", Type: lexer.Keyword}},
	}

	tokens := c.Tokens()
	r.Equal(3, len(tokens))
	s := lexer.Write(tokens)
	r.Equal("SYSDATE,\nCURRENT_TIMESTAMP", s)
}

func TestSelect(t *testing.T) {
	r := require.New(t)
	var s ast.Node = ast.Select{
		Select: lexer.Token{Raw: "SELECT", Type: lexer.Keyword},
		List:   ast.Atom{lexer.Token{Prefix: " ", Raw: "SYSDATE", Type: lexer.Keyword}},
	}
	_ = s.(ast.Select) // Type assertion.

	tokens := s.Tokens()
	r.Equal(2, len(tokens))

	sql := lexer.Write(tokens)
	r.Equal("SELECT SYSDATE", sql)
}
