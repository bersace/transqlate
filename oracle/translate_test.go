package oracle_test

import (
	"fmt"
	"log/slog"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/oracle"
)

func Example() {
	// Translate a query from Oracle to PostgreSQL
	sql, err := oracle.Translate("<stdin>", "SELECT sysdate FROM DUAL;")
	if err != nil {
		panic(err)
	}
	fmt.Println(sql)
	// Output: SELECT CURRENT_TIMESTAMP;
}

func TestMain(m *testing.M) {
	slog.SetDefault(slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
		Level: slog.LevelDebug,
	})))

	os.Exit(m.Run())
}

func TestNochanges(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT 1;")
	r.NoError(err)
	r.Equal("SELECT 1;", sql)
}

func TestSysdateFromDual(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT sysdate FROM DUAL;")
	r.NoError(err)
	r.Equal("SELECT CURRENT_TIMESTAMP;", sql)
}

func TestRemoveLastToDateArgument(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT TO_DATE('2018-01-01', 'YYYY-MM-DD', 'NLS_DATE_LANGUAGE=ENGLISH') FROM DUAL;")
	r.NoError(err)
	r.Equal("SELECT TO_DATE('2018-01-01', 'YYYY-MM-DD');", sql)
}
