package oracle

import (
	"fmt"

	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/parser"
	"gitlab.com/dalibo/transqlate/rewrite"
)

// Translate transpile Oracle SQL into Postgres SQL.
func Translate(source, sql string) (string, error) {
	tree, err := parser.Parse(lexer.New(source, sql))
	if err != nil {
		return "", fmt.Errorf("parse: %w", err)
	}
	tree = rewrite.Do(tree, PostgresRules)
	sql = lexer.Write(tree.Tokens())
	return sql, nil
}
