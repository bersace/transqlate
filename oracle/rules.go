package oracle

import (
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/rewrite"
)

// PostgresRules is a set of rules to rewrite Oracle Dialect into Postgres Dialect.
var PostgresRules = []rewrite.Rule{
	// Order matters here.
	rewrite.RewriteConstant{From: "sysdate", To: "CURRENT_TIMESTAMP"},
	RemoveFromDual{},
	rewrite.RemoveLastArgument{Function: "to_date"},
}

type RemoveFromDual struct{}

func (d RemoveFromDual) String() string {
	return "remove from dual"
}

func (d RemoveFromDual) Match(n ast.Node) bool {
	s, ok := n.(ast.Select)
	if !ok {
		return false
	}
	f, ok := s.From.(ast.From)
	if !ok {
		return false
	}
	l, ok := f.List.(ast.Atom)
	if !ok {
		return false
	}
	if l.Token.Type != lexer.Identifier {
		return false
	}

	return l.Token.Str == "dual"
}

func (d RemoveFromDual) Rewrite(node ast.Node) ast.Node {
	s := node.(ast.Select)
	s.From = nil
	return s
}
